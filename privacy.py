#!/usr/bin/env python3
import os, datetime
import argparse
import urllib.request as request
import urllib.parse as parse
import json

API_FILE='~/.pinboardprivacy'

DAYS = 90

API_URL = 'https://api.pinboard.in/v1/'

TIME_FMT = '%Y-%m-%dT%H:%M:%SZ'

class API:

    def __init__(self, args):
        self.api_token = open(args.token_file).read().strip()

    def _get_result(self, function, args):
        args['auth_token'] = self.api_token
        args['format'] = 'json'
        url = parse.urljoin(API_URL, function) + '?' + parse.urlencode(args)
        response = request.urlopen(url)
        encoding = response.headers.get_content_charset()
        return json.loads(response.read().decode(encoding))

    def fetch_bookmarks_before(self, todt):
        todt_str = todt.strftime(TIME_FMT)
        args = {
            'todt': todt_str,
        }
        return self._get_result('posts/all', args)

    def lock_bookmark(self, bookmark):
        args = {
            'url': bookmark['href'],
            'description': bookmark['description'],
            'extended': bookmark['extended'],
            'dt': bookmark['time'],
            'replace' : 'yes',
            'shared': 'no',
            'toread': bookmark['toread'],
            'tags': bookmark['tags'],
        }

        return self._get_result('posts/add', args)

def main():

    parser = argparse.ArgumentParser(
            description='Make old pinboard bookmarks private')
    parser.add_argument('--days', '-d', metavar='DAYS', type = int,
            help = 'Lock bookmarks older than this number of days (default %d)'
                    % DAYS,
            default = DAYS)
    parser.add_argument('--token-file', '-f', metavar='FILENAME',
            help = 'File containing Pinboard API token (default %s)'
                    % API_FILE,
            default = os.path.expanduser(API_FILE))
    args = parser.parse_args()

    api = API(args)

    lock_before = datetime.datetime.now() - datetime.timedelta(days = args.days)

    for bookmark in api.fetch_bookmarks_before(lock_before):
        if bookmark['shared'] == 'yes':
            response = api.lock_bookmark(bookmark)
            if response['result_code'] == 'done':
                print("Locked Pinboard link for %s" % bookmark['href'])
            else:
                print("Error locking Pinboard link for %s" % bookmark['href'])
            
if __name__ == '__main__':
    main()
