Unmaintained: Pinboard Privacy Lock
-----------------------------------

Pinboard Privacy Lock marks a user's [Pinboard](https://pinboard.in/) bookmarks
older than a certain number of days (default: 90) as private.

This software is no longer maintained. Bug reports / patches will not be accepted.

Requirements
-------------

* Python 3

Setup
-----

1. Obtain your API token (which has the form `username:NNNNNNNNNNNNNNNNNNNN`)
from [the Pinboard settings page](https://pinboard.in/settings/password).
1. Save the API token in a file called `.pinboardprivacy` in your home directory.

Usage
-----

    usage: privacy.py [-h] [--days DAYS] [--token-file FILENAME]
    
    Make old pinboard bookmarks private
    
    optional arguments:
      -h, --help            show this help message and exit
      --days DAYS, -d DAYS  Lock bookmarks older than this number of days (default
                            90)
      --token-file FILENAME, -f FILENAME
                            File containing Pinboard API token (default
                            ~/.pinboardprivacy)

Frequency
---------

This script calls the [`posts/all` Pinboard API
method](https://pinboard.in/api/#posts_all) and as such should be run no more
often than every five minutes.

Caveats
-------

This software uses [Pinboard API v1](https://pinboard.in/api/). As it is
unmaintained, it will not be ported to
[v2](https://pinboard.in/api/v2/overview) should v2 ever be released.

About
=====

Status
------

This software is no longer maintained. Bug reports / patches will not be accepted.

Credits
-------

Pinboard Privacy Lock was developed by [Mary
Gardiner](https://mary.gardiner.id.au/).

Licence
-------

Pinboard Privacy Lock is free software available under the MIT licence. See
LICENCE for full details.
